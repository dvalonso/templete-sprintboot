# Template Springboot


## ¡Importante!

* Java 11
* PostgreSQL

## Recomendaciones

Uso de SDK Man para cambiar entre versiones de java
* https://sdkman.io/

## Observabilidad

* https://www.baeldung.com/spring-boot-actuators
* https://www.baeldung.com/micrometer
* https://refactorfirst.com/spring-boot-prometheus-grafana
* https://www.baeldung.com/micrometer

## Testing


JUnit 5  
* https://www.baeldung.com/junit-5
* https://junit.org/junit5/

Mockito
* https://www.baeldung.com/mockito-series
* https://site.mockito.org/

Mockmvc
* https://blog.marcnuri.com/mockmvc-introduccion-a-spring-mvc-testing
* https://www.baeldung.com/integration-testing-in-spring

Sonarqube
* https://www.baeldung.com/sonar-qube
***

